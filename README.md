experience with WebRTC

# WebWinRTC

## 0. known Variations:

|Name|Remark|Language|Plattform|Tutorial|API Document|Download|
|----|----|----|----|----|----|----|
|WebRTC|Original-WebRTC|unknown|Web or Http|unknown|unknown|unknown|
|MRTK-WebRTC|MRTK-WebRTC|c\#|Win32, UWP, Unity|https://microsoft.github.io/MixedReality-WebRTC/manual/introduction.html|https://microsoft.github.io/MixedReality-WebRTC/api/Microsoft.MixedReality.WebRTC.html |https://github.com/microsoft/MixedReality-WebRTC|
|WinRTC|WinRTC|C++|UWP...|https://www.youtube.com/watch?v=GKrTmgZT-EA&feature=youtu.be|unknown|https://github.com/microsoft/winrtc|



## 1. Literature and Standards of WebRTC: 
@see: https://tools.ietf.org/wg/rtcweb/

## 2. Introduction of MRTK-WebRTC in C\#(Win32), C\#(UWP) and Unity

@see: https://microsoft.github.io/MixedReality-WebRTC/manual/introduction.html

## 3. API Documentation of MRTK-WebRTC
@see: https://microsoft.github.io/MixedReality-WebRTC/api/Microsoft.MixedReality.WebRTC.html 
